from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Attendee, AccountVO, ConferenceVO
import json
from django.views.decorators.http import require_http_methods


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
    ]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }

    def get_extra_data(self, o):
        count = AccountVO.objects.filter(email=o.email).count()
        if count > 0:
            return {"has_account": True}
        else:
            return {"has_account": False}


# This funtion is the earlier version of the list view, re-written
# and preserved to help with learning and notes-taking
# def api_list_attendees(request, conference_id):
#     attendees = Attendee.objects.filter(conference=conference_id)
#     response = [
#         {
#             "name": attendee.name,
#         }
#         for attendee in attendees
#     ]
#     return JsonResponse({"response": response})


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id):
    try:
        conference_href = f"/api/conferences/{conference_vo_id}/"
        print(conference_href)
        conference = ConferenceVO.objects.get(import_href=conference_href)
    except ConferenceVO.DoesNotExist:
        return JsonResponse({"message": "Invalid conference id"}, status=400)
    if request.method == "POST":
        content = json.loads(request.body)
        content["conference"] = conference
        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee, encoder=AttendeeDetailEncoder, safe=False
        )
    else:
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, id):
    try:
        attendee = Attendee.objects.get(id=id)
    except Attendee.DoesNotExist:
        return JsonResponse({"message": "Invalid attendee id"}, status=400)
    if request.method == "DELETE":
        deleted, _ = Attendee.objects.get(id=id).delete()
        return JsonResponse({"deleted": deleted > 0})
    elif request.method == "GET":
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        if "conference" in content:
            try:
                content["conference"] = ConferenceVO.objects.get(
                    id=content["conference"]
                )
            except ConferenceVO.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid conference id"}, status=400
                )
        Attendee.objects.filter(id=id).update(**content)
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee, encoder=AttendeeDetailEncoder, safe=False
        )
