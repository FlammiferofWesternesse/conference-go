from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from .acl import get_city_picture, get_location_weather

import json
from .models import (
    Conference,
    Location,
    State,
)


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
    ]


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
    ]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "state",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "POST":
        content = json.loads(request.body)
        try:
            content["location"] = Location.objects.get(id=content["location"])
        except Location.DoesNotExist:
            return JsonResponse({"message": "Invalid location id"}, status=400)
        conferences = Conference.objects.create(**content)
        return JsonResponse(
            conferences,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    else:
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, id):
    try:
        conference = Conference.objects.get(id=id)
    except Conference.DoesNotExist:
        return JsonResponse({"message": "Invalid conference id"}, status=400)
    if request.method == "DELETE":
        deleted, _ = Conference.objects.get(id=id).delete()
        return JsonResponse({"deleted": deleted > 0})
    elif request.method == "GET":
        weather = get_location_weather(
            conference.location.city, conference.location.state.abbreviation
        )
        return JsonResponse(
            {"weather": weather, "conference": conference},
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        if "location" in content:
            try:
                content["location"] = Location.objects.get(
                    id=content["location"]
                )
            except Location.DoesNotExist:
                return JsonResponse({"message": "Invalid location id"})
        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "POST":
        content = json.loads(request.body)
        try:
            state = State.objects.get(abbreviation=content["state"])
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        content["state"] = state
        content.update(
            get_city_picture(content["city"], content["state"].name)
        )
        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    else:
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    try:
        location = Location.objects.get(id=id)
    except Location.DoesNotExist:
        JsonResponse({"message": "Invalid location id"}, status=400)
    if request.method == "DELETE":
        deleted, _ = Location.objects.get(id=id).delete()
        return JsonResponse({"deleted": deleted > 0})
    elif request.method == "GET":
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            if "state" in content:
                content["state"] = State.objects.get(
                    abbreviation=content["state"]
                )
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        if location.picture_url is None and (
            location.city is not None and location.state.name is not None
        ):
            content.update(
                get_city_picture(location.city, location.state.name)
            )
        Location.objects.filter(id=id).update(**content)
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
