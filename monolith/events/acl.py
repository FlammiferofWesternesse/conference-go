import requests
from .keys import PEXELS_API_KEY, OPENWEATHER_API_KEY
import json


def get_location_weather(city, state):
    geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&limit=1&appid={OPENWEATHER_API_KEY}"
    response = requests.get(geo_url)
    geo_content = json.loads(response.content)
    lat, lon = (
        geo_content[0]["lat"],
        geo_content[0]["lon"],
    )
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPENWEATHER_API_KEY}&units=imperial"
    response = requests.get(weather_url)
    weather_content = json.loads(response.content)
    weather = {
        "temp": weather_content["main"]["temp"],
        "description": weather_content["weather"][0]["description"],
    }
    return weather


def get_city_picture(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query": f"{city} {state}"}
    url = "https://api.pexels.com/v1/search"
    response = requests.get(
        url=url,
        params=params,
        headers=headers,
    )
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}
